# React Single Page Application (SPA) Starter #

Just clone and use :) Please link back if you can.


### Why? ###
Because in 2017 the land of Front End Web Development is stupidly confusing. I mean if you are new, does the following sound like you? 

YOU: "I wanna learn React because I hear it's (insert cool reason for learning it)!"
ME: "Sure. Just import a JS transpiler and React libraries through NPM, configure Webpack to assemble and test your transpiled code into an application and you should be good to go!"
YOU: "Uuuhh... whaa..?"

Because that was me.

I spent a good two months learning how to put everything together that you see here and this is just a small subset of all the available libraries out there.

I put this together because:
1) "I WANNA LEARN REACT BECAUSE I HEAR IT'S --" (insert cool reasons)
2) I wanted a ready to run base code that I can quickly experiment React concepts with.
3) I wanted a ready to run base code that gives me a fast feedback loop of when I break things.
4) I wanted a ready to run base code that I can deploy quickly to production with for those rapid prototyping concepts.

The base code should be easy enough for (newb or veteran) anyone to run and tinker with. Some might say it's a bit TOO simple and if that's the case, by all means make a pull request.

I intend on updating the code and the libraries used as I go for the benefit of any new person stumbling on this repository and want to learn and play with the latest React.

There aren't any tutorials here I'm afraid. If you want a great tutorial, give Steven Grider at Udemy a go. I learnt from him.


### What is in this Starter? ###
This starter uses Babel transpiler to translate Stage-1, ES2015, React JSX to regular ES5 JavaScript.
Webpack Dev Server is preconfigured for local development testing.
Semantic UI and CSS has been added because I like the clean, simple aesthetics as well as useful components of its library.
Vendor and Bundle Code is pre-split and hashed for cache busting. Images are automatically included.
Rapid live production deployment (for fast user feedback) is handled by Surge.sh. Please set your own surge subdomain. I've already claimed reactspastarter.surge.sh! :p


### How do I get set up? ###
After cloning this repository and running 'npm install' you can run any of the following commands:

`'npm run build'`  
All the stuff in src is transpiled, uglified, split, chunked and hashed into a 'dist' folder. If 'dist' folder exists, it will be erased. If it doesn't it will be created.

`'npm run clean'`  
Will wipe out the 'dist' folder.

`'npm run start'`  
Webpack Dev Server will launch your code. Viewable on 'http://localhost:8080/'. Will throw errors if something is already running on port 8080.

`'npm run surgeDeploy'`  
MAKE SURE YOU CHANGE THE SUBDOMAIN TO SOMETHING ELSE IN 'package.json'.
Will clean your 'dist' folder, build your src folder and deploy it to surge. If you don't already have a Surge account, there will be commands to create one.

`'npm run surgeList'`  
MAKE SURE YOU CHANGE THE SUBDOMAIN TO SOMETHING ELSE IN 'package.json'.
Lists your surge subdomains if you're logged in.

`'npm run surgeTeardown'`  
MAKE SURE YOU CHANGE THE SUBDOMAIN TO SOMETHING ELSE IN 'package.json'.
Will attempt to delete this Surge deployment if you're logged in and are authorised.

`'npm run surgeLogout'`  
MAKE SURE YOU CHANGE THE SUBDOMAIN TO SOMETHING ELSE IN 'package.json'.
Logs you out of Surge.

`'npm run surgeLogin'`  
MAKE SURE YOU CHANGE THE SUBDOMAIN TO SOMETHING ELSE IN 'package.json'.
Logs you into Surge.

`'npm update'`  
Updates the packages in package.json to the latest available.

### .env ###
You need to add this .env file in the root directory yourself if you want to expose custom environment variables for you app to use. I'm using dotenv-webpack to pull in the env file. The name of the file can be configured in webpack.config.js. The .env is already ignored for you in the .gitignore as it's considered a security risk to store API keys in code.

### Who do I talk to? ###
Anyone but me.

LOL Just kidding. You can ask me stuff but I'm pretty new to this myself. I can explain parts of this Starter repository to you if it's not clear but if you're looking for a specific way of doing something you're honestly better off going to Google or StackOverflow...

### Suggestions for cool stuff to try ###
If you're wondering what you can build with this starter app, consider trying Firebase by Google. It's a back-end/server-side platform that interfaces quite nicely with modern SPAs and especially this starter repo. It's got Hosting services (like Surge!), Authentication services and even a Database for you play with!

I've done a bit of work with Firebase before so if you have some questions, feel free to ask!