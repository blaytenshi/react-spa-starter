const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const Dotenv = require('dotenv-webpack');

const VENDOR_LIBS = [
    'firebase',
    'react',
    'react-dom',
    'react-input-range',
    'react-redux',
    'react-router',
    'redux',
    'redux-form',
    'redux-thunk',
    'semantic-ui-css',
    'semantic-ui-react'
];

module.exports = {
    entry: {
        bundle: './src/index.js',
        vendor: VENDOR_LIBS
    },
    output: {
        path: path.join(__dirname, 'dist'),
        filename: '[name].[chunkhash].js' //chunkhash is a string that is the hashed content of the file. We're using this to uniquely identify the file.
    },
    resolve: {
        modules: [
            path.resolve('src'),
            'node_modules'
        ],
        extensions: ['.js', '.jsx'],
    },
    module: {
        rules: [
            {
                use: 'babel-loader',
                test: /\.js$/,
                exclude: /node_modules/
            },
            {
                use: ExtractTextPlugin.extract({
                    use: 'css-loader'
                }),
                test: /\.css$/
            },
            {
                test: /\.(png|jpe?g|gif|svg|eot|ttf|woff|woff2)$/,
                loader: 'url-loader',
                options: {
                    limit: 10000
                }
            }
        ]
    },
    plugins: [
        new Dotenv({
            path: './.env' // .env file containing all your API keys and stuff
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: ['vendor', 'manifest'] //adding an extra manifest file in there
        }),
        new HtmlWebpackPlugin({
            template: 'src/index.html' //manually add in a script tag for every js file we need to include (bundle and vendor js files)
        }),
        new ExtractTextPlugin('style.css')
    ]
};
