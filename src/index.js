/**
 * Created by jtang on 17/7/17.
 */
import React from 'react';
import ReactDOM from 'react-dom';

const App = () => {
    return (
        <div>
            <h1>Hi!</h1>
            <p>This is a demo of a starter single page application built with react!</p>
            <p>For more information, please check out:</p>
            <a href="https://bitbucket.org/blaytenshi/react-spa-starter" target="_self">https://bitbucket.org/blaytenshi/react-spa-starter</a>
        </div>
    );
}

ReactDOM.render(<App />, document.getElementById('root'));